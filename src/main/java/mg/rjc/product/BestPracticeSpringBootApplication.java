package mg.rjc.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class BestPracticeSpringBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(BestPracticeSpringBootApplication.class, args);
    }

}
