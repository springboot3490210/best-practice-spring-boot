package mg.rjc.product.handler;

import mg.rjc.product.dto.ApiResponse;
import mg.rjc.product.dto.ErrorDTO;
import mg.rjc.product.exception.ProductNotFoundException;
import mg.rjc.product.exception.ProductServiceBusinessException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RestControllerAdvice
public class ProductServiceExceptionHandler {

    private final static String FAILED = "FAILED";

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiResponse<?> handleMethodArgumentException(MethodArgumentNotValidException exception) {
        List<ErrorDTO> errors = new ArrayList<>();
        exception.getBindingResult().getFieldErrors().forEach(error -> {
            ErrorDTO errorDTO = new ErrorDTO(error.getField(), error.getDefaultMessage());
            errors.add(errorDTO);
        });
        return ApiResponse.builder()
                .status(FAILED)
                .errors(errors)
                .build();
    }

    @ExceptionHandler(ProductServiceBusinessException.class)
    public ApiResponse<?> handleServiceException(ProductServiceBusinessException exception) {
        return ApiResponse.builder()
                .status(FAILED)
                .errors(Collections.singletonList(new ErrorDTO("", exception.getMessage())))
                .build();
    }

    @ExceptionHandler(ProductNotFoundException.class)
    public ApiResponse<?> handleProductNotFoundException(ProductNotFoundException exception) {
        return ApiResponse.builder()
                .status(FAILED)
                .errors(Collections.singletonList(new ErrorDTO("", exception.getMessage())))
                .build();
    }

}
