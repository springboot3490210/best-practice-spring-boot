package mg.rjc.product.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mg.rjc.product.dto.ApiResponse;
import mg.rjc.product.dto.ProductRequestDTO;
import mg.rjc.product.dto.ProductResponseDTO;
import mg.rjc.product.mapper.ProductMapper;
import mg.rjc.product.service.ProductService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "api/products")
@RequiredArgsConstructor
@Slf4j
public class ProductRestController {

    private static final String SUCCESS = "success";
    private final ProductService productService;

    @PostMapping
    public ResponseEntity<ApiResponse> createNewProduct(@RequestBody @Valid ProductRequestDTO productRequestDTO){
        log.info("ProductRestController::createNewProduct request body {}", ProductMapper.jsonAsString(productRequestDTO));
        final ProductResponseDTO productResponseDTO = productService.createNewProduct(productRequestDTO);

        // Builder Design pattern
        ApiResponse<ProductResponseDTO> responseDTO = ApiResponse.<ProductResponseDTO>builder()
                .status(SUCCESS)
                .data(productResponseDTO)
                .build();

        log.info("ProductRestController::createNewProduct response {}", ProductMapper.jsonAsString(responseDTO));
        return new ResponseEntity<>(responseDTO, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<ApiResponse> getProducts() {
        List<ProductResponseDTO> productResponseDTOS = productService.getProducts();
        ApiResponse<List<ProductResponseDTO>> responseDTO = ApiResponse.<List<ProductResponseDTO>>builder()
                .status(SUCCESS)
                .data(productResponseDTOS)
                .build();
        log.info("ProductRestController::getProducts response {}", ProductMapper.jsonAsString(responseDTO));
        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }

    @GetMapping(path = "/{productId}")
    public ResponseEntity<ApiResponse> getProduct(@PathVariable Long productId) {
        log.info("ProductRestController::getProduct by id {}", productId);
        ProductResponseDTO productResponseDTO = productService.getProductById(productId);
        ApiResponse<ProductResponseDTO> responseDTO = ApiResponse.<ProductResponseDTO>builder()
                .status(SUCCESS)
                .data(productResponseDTO)
                .build();

        log.info("ProductRestController::getProduct by id {} response {} ", productId, ProductMapper.jsonAsString(responseDTO));
        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }

    @GetMapping(path = "/types")
    public ResponseEntity<ApiResponse> getProductByTypes() {
        Map<String, List<ProductResponseDTO>> productMaps = productService.getProductsByTypes();
        ApiResponse<Map<String, List<ProductResponseDTO>>> responseDTO = ApiResponse.<Map<String, List<ProductResponseDTO>>>builder()
                .status(SUCCESS)
                .data(productMaps)
                .build();
        log.info("ProductRestController::getProductByTypes response {}", ProductMapper.jsonAsString(responseDTO));
        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }
}
