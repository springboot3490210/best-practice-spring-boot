package mg.rjc.product.mapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import mg.rjc.product.dto.ProductRequestDTO;
import mg.rjc.product.dto.ProductResponseDTO;
import mg.rjc.product.entity.Product;

public class ProductMapper {

    public static Product convertToEntity(ProductRequestDTO productRequestDTO) {
        return Product.builder()
                .name(productRequestDTO.getName())
                .description(productRequestDTO.getDescription())
                .productType(productRequestDTO.getProductType())
                .quantity(productRequestDTO.getQuantity())
                .price(productRequestDTO.getPrice())
                .supplierName(productRequestDTO.getSupplierName())
                .supplierCode(productRequestDTO.getSupplierCode())
                .build();
    }

    public static ProductResponseDTO convertToDTO(Product product) {
        return ProductResponseDTO.builder()
                .id(product.getId())
                .name(product.getName())
                .description(product.getDescription())
                .productType(product.getProductType())
                .quantity(product.getQuantity())
                .price(product.getPrice())
                .supplierName(product.getSupplierName())
                .supplierCode(product.getSupplierCode())
                .build();
    }

    public static String jsonAsString(Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        }catch (JsonProcessingException e){
            e.printStackTrace();
        }
        return null;
    }
}
