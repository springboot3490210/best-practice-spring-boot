package mg.rjc.product.service;

import mg.rjc.product.dto.ProductRequestDTO;
import mg.rjc.product.dto.ProductResponseDTO;
import mg.rjc.product.exception.ProductServiceBusinessException;

import java.util.List;
import java.util.Map;

public interface ProductService {
    ProductResponseDTO createNewProduct(ProductRequestDTO productRequestDTO) throws ProductServiceBusinessException;
    List<ProductResponseDTO> getProducts() throws ProductServiceBusinessException;
    ProductResponseDTO getProductById(Long productId);
    Map<String, List<ProductResponseDTO>> getProductsByTypes();
}
