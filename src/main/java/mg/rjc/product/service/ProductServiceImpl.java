package mg.rjc.product.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mg.rjc.product.dto.ProductRequestDTO;
import mg.rjc.product.dto.ProductResponseDTO;
import mg.rjc.product.entity.Product;
import mg.rjc.product.exception.ProductNotFoundException;
import mg.rjc.product.exception.ProductServiceBusinessException;
import mg.rjc.product.mapper.ProductMapper;
import mg.rjc.product.repository.ProductRepository;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    /**
     * This method create a new product in the database
     *
     * @param productRequestDTO ProductRequestDTO
     * @return ProductResponseDTO
     * @throws ProductServiceBusinessException
     */
    @Override
    public ProductResponseDTO createNewProduct(ProductRequestDTO productRequestDTO) throws ProductServiceBusinessException {
        ProductResponseDTO productResponseDTO;
        try {
            log.info("ProductService::createNewProduct execution started.");
            final Product product = ProductMapper.convertToEntity(productRequestDTO);
            log.debug("ProductService::createNewProduct request parameters {}", ProductMapper.jsonAsString(productRequestDTO));
            final Product productResult = productRepository.save(product);
            productResponseDTO = ProductMapper.convertToDTO(productResult);
            log.debug("ProductService::createNewProduct received response from database {}", ProductMapper.jsonAsString(productRequestDTO));
        } catch (Exception ex) {
            log.error("Exception occurred while persisting product to database, Exception message {}", ex.getMessage());
            throw new ProductServiceBusinessException("Exception occurred while create a new Product");
        }
        log.info("ProductService::createNewProduct execution ended.");
        return productResponseDTO;
    }

    /**
     * This method fetch all product from database
     *
     * @return List<Product>
     * @throws ProductServiceBusinessException
     */
    @Override
    @Cacheable(value = "product")
    public List<ProductResponseDTO> getProducts() throws ProductServiceBusinessException {
        List<ProductResponseDTO> productResponseDTOS;
        try{
            log.info("ProductService::getProducts execution started");
            List<Product> productList = productRepository.findAll();
            if(!productList.isEmpty()){
                productResponseDTOS = productList.stream()
                        .map(ProductMapper::convertToDTO)
                        .collect(Collectors.toList());
            } else {
                productResponseDTOS = Collections.emptyList();
            }
            log.debug("ProductService::getProducts retrieving products from database {}", ProductMapper.jsonAsString(productResponseDTOS));
        } catch (Exception ex) {
            log.error("Exception occurred while retrieving products from database , Exception message {}", ex.getMessage());
            throw new ProductServiceBusinessException("Exception occurred while fetch all products from Database");
        }
        log.info("ProductService::getProducts execution ended");
        return productResponseDTOS;
    }

    /**
     * This method will fetch product from database by id
     *
     * @param productId
     * @return Product response from database
     */
    @Override
    @Cacheable(value = "product")
    public ProductResponseDTO getProductById(Long productId) {
        ProductResponseDTO productResponseDTO;
        try{
            log.info("ProductService::getProductById execution started");
            Product product = productRepository.findById(productId)
                    .orElseThrow(() -> new ProductNotFoundException("Product not found with id " + productId ));
            productResponseDTO = ProductMapper.convertToDTO(product);
            log.debug("ProductService:getProductById retrieving product from database for id {} {}", productId, ProductMapper.jsonAsString(productResponseDTO));
        } catch (Exception ex) {
            log.error("Exception occurred while retrieving product {} from database , Exception message {}", productId, ex.getMessage());
            throw new ProductServiceBusinessException("Exception occurred while fetch product from Database " + productId);
        }
        log.info("ProductService::getProductById execution ended.");
        return productResponseDTO;
    }

    /**
     * This method fetch all product from database and groupingBy productTypes
     *
     * @return Map<String, List<Product>>
     */
    @Override
    @Cacheable(value = "product")
    public Map<String, List<ProductResponseDTO>> getProductsByTypes() {
        try{
            log.info("ProductService::getProductsByTypes execution started.");
            Map<String, List<ProductResponseDTO>> productMaps = productRepository.findAll().stream()
                    .map(ProductMapper::convertToDTO)
                    .filter(productResponseDTO -> productResponseDTO.getProductType() != null)
                    .collect(Collectors.groupingBy(ProductResponseDTO::getProductType));
            log.info("ProductService::getProductsByTypes execution ended.");
            return productMaps;
        } catch (Exception ex) {
            log.error("Exception occurred while retrieving product grouping by type from database , Exception message {}", ex.getMessage());
            throw new ProductServiceBusinessException("Exception occurred while fetch product from Database");
        }
    }
}
