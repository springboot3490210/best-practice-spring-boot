package mg.rjc.product.dto;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import lombok.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Builder
public class ProductRequestDTO {

    @NotBlank(message = "Product name shouldn't be NULL or EMPTY")
    private String name;

    private String description;

    @NotBlank(message = "Product type shouldn't be NULL or EMPTY")
    private String productType;

    @Min(value = 1, message = "Quantity is not defined !")
    private int quantity;

    @Min(value = 200, message = "Product price can't be less than 200")
    @Max(value = 500000, message = "Product price can't more than 500000")
    private double price;

    private String supplierName;

    @NotBlank(message = "Supplier code shouldn't be NULL or EMPTY")
    private String supplierCode;
}
